Documentazione FUSS - Guida per sviluppatori
============================================

Qui puoi trovare la documentazione per la manutenzione e lo sviluppo di FUSS.

La documentazione è scritta in `reStructuredText
<http://www.sphinx-doc.org/rest.html>`_ e pubblicata su `readthedocs
<https://readthedocs.org/projects/fuss-dev-guide/>`_.

Contribuisci
------------

Chiunque può contribuire a migliorare la documentazione.

Istruzioni su come fare sono disponibili `sulla guida di sviluppo
<https://fuss-dev-guide.readthedocs.io/it/latest/guide-progetto-fuss.html>`_

Supporto
--------

Se ti serve aiuto, scrivi una mail ad info@fuss.bz.it

Licenze
-------

.. image:: https://img.shields.io/badge/code-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.svg
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA

.. image:: https://img.shields.io/badge/screenshots-CC0-ff69b4.svg
    :target: https://creativecommons.org/publicdomain/zero/1.0/
    :alt: Screenshots CC0
