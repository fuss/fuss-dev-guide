**************
Appunti su Git
**************

Lo sviluppo di fuss viene gestito tramite repository git pubblicati
sull'istanza gitlab https://gitlab.fuss.bz.it. Per le basi dell'uso di
git si rimanda ai link presenti nella sezione `Vedere anche`_, ma questo
documento contiene indicazioni su come svolgere alcuni compiti
specifici.

.. contents::

Branch per il supporto di più distribuzioni
===========================================

Lo sviluppo nel branch ``master`` si riferisce sempre alla distribuzione
più recente tra quelle supportate per quel pacchetto; qualora vengano
supportate anche distribuzioni più vecchie tale supporto avviene in un
branch chiamato con il nome della distribuzione (ad esempio ``buster``).

.. warning::

   In alcuni pacchetti il nome del branch è ancora nella forma
   ``fuss/<distribuzione>``: questo è da evitare, perché conflitta con
   il nome del branch usato per la pacchettizzazione in un branch
   separato, ma ancora non è stato interamente uniformato.

Modifiche da applicare a tutte le distribuzioni
-----------------------------------------------

Nel caso in cui si debbano fare modifiche che devono essere presenti in
tutte le versioni, la buona pratica è di effettuare la modifica nella
versione più recente (branch ``master``), la si committi e quindi si
faccia il cherry pick del commit sul branch delle versioni più vecchie.

Ad esempio, essendo nella seguente situazione::

   fuss-foo (master)$ git log --graph --abbrev-commit --pretty=oneline
   * 7bc3fd2 (HEAD -> master) Start working on bullseye
   * 0f46c37 (buster) Readme for the fuss-foo project

Si fa una modifica, la si committa con il comando::

   fuss-foo (master)$ git commit -m 'Add project description.'

E ci si trova nella situazione seguente::

   fuss-foo (master)$ git log --graph --abbrev-commit --pretty=oneline
   * 36c12d2 (HEAD -> master) Add project description.
   * 7bc3fd2 Start working on bullseye
   * 0f46c37 (buster) Readme for the fuss-foo project

A questo punto passiamo sul branch della vecchia distribuzione, e
facciamo ``cherry-pick`` del commit, ovvero lo copiamo nel nuovo
branch::

   fuss-foo (master)$ git checkout buster
   fuss-foo (buster)$ git cherry-pick 36c12d2
   Auto-merging README.rst
   CONFLICT (content): Merge conflict in README.rst
   error: could not apply 36c12d2... Add project description.
   hint: After resolving the conflicts, mark them with
   hint: "git add/rm <pathspec>", then run
   hint: "git cherry-pick --continue".
   hint: You can instead skip this commit with "git cherry-pick --skip".
   hint: To abort and get back to the state before "git cherry-pick",
   hint: run "git cherry-pick --abort".

Purtroppo non è andato tutto bene e la modifica non si applica in modo
pulito; apriamo il file ``README.rst`` e troviamo quanto segue::

   Fuss FOO
   ========

   <<<<<<< HEAD
   This is FOO for buster
   =======
   This is FOO for bullseye

   It is an example project.
   >>>>>>> 36c12d2 (Add project description.)

risolviamo il conflitto manualmente, portando i contenuti ad essere::

   Fuss FOO
   ========

   This is FOO for buster

   It is an example project.

e infine proseguiamo il ``cherry-pick`` come suggerito dal messaggio di
errore::

   fuss-foo (buster)$ git add README.rst
   fuss-foo (buster)$ git cherry-pick --continue
   [...]
   [buster c21a51e] Add project description.
    Date: Fri Jun 10 11:03:35 2022 +0200
    1 file changed, 2 insertions(+)

Confermando il messaggio di commit nell'editor che si è aperto.

A questo punto la situazione è diventata::

   fuss-foo (buster)$ git log --graph --abbrev-commit --pretty=oneline --all
   * c21a51e (HEAD -> buster) Add project description.
   | * 36c12d2 (master) Add project description.
   | * 7bc3fd2 Start working on bullseye
   |/
   * 0f46c37 Readme for the fuss-foo project

che è quanto desideravamo.

.. note::

   Nella maggior parte dei casi git è abbastanza furbo ed è raro
   trovarsi nella situazione sopra in cui il cherry-pick richiede
   intervento manuale per risolvere conflitti, tranne che in un caso
   specifico: il file ``debian/changelog``.

   Se si tiene la pacchettizzazione debian dentro ad un branch separato
   questo non è un problema, ma se si ha ancora la pacchettizzazione
   assieme al codice, ci si trova in una situazione in cui non c'è una
   buona soluzione.

   Se si mettono le modifiche a ``debian/changelog`` nello stesso commit
   in cui le modifiche sono state fatte si ottiene una storia del
   repository più pulita, ma si ha la certezza di trovare a dover
   risolvere un conflitto in fase di cherry-pick.

   Committare invece ``debian/changelog`` a parte previene quei
   conflitti, ma lascia una storia più sporca, con almeno due commit per
   ciascuna modifica: non è un gran problema quando ad una riga di
   changelog corrispondono magari una decina di commit di uno sviluppo
   importante, ma quando una riga di changelog corrisponde ad un solo
   semplice commit, come spesso è il caso, può essere fastidioso.

Vedere anche
============

* `Pro Git di Scott Chacon <https://git-scm.com/book>`_ libro
  consultabile online.
* `Git Happens di Jessica Kerr
  <https://www.youtube.com/watch?v=yCh6TSLIQBQ>`_ video di un talk
  introduttivo a git.
