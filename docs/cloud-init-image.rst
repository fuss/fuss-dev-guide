************************************
 Creazione dell'immagine cloud-init
************************************

.. contents::

A partire da FUSS 10 (Debian Buster) si genereranno delle immagini complete
per macchina virtuale del server nel formato dei dump della piattaforma
Proxmox adottata dal progetto, in modo da semplificare l'installazione e la
configurazione iniziale di un FUSS server.

Le immagini si appoggiano a ``cloud-init`` in modo da consentire la gestione
della rete direttamente dall'interfaccia web, una volta che le si siano
importate e gli si sia associato un volume per ``cloud-init`` (i dettagli sono
illustrati nella fuss-tech-guide.

Creazione macchina virtuale
===========================

Si crei una nuova macchina virtuale (VM) su Proxmox, in fase iniziale è
sufficiente l'interfaccia di rete singola creata dal wizard, si usi il bridge
associato alla rete esterna.

Sulla macchina virtuale si deve fare una installazione ordinaria di Debian con
*netinstall* (ci si procuri l'ultima versione della ISO e la si carichi dentro
``/var/lib/vz/template/iso``)::

  # cd /var/lib/vz/template/iso/
  # wget https://cdimage.debian.org/cdimage/bookworm_di_rc4/amd64/iso-cd/debian-bookworm-DI-rc4-amd64-netinst.iso

Nella creazione si utilizzi la precedente immagine come CDROM e per il resto
si usino i valori di default per tutto, compresi i 32 G di dimensione del
disco, avendo però cura di fare installare quest'ultimo sullo storage
``local`` in formato *qcow* (che consente eventualmente di distribuire
direttamente il file che si otterrà come immagine del disco per l'uso da parte
di altre piattaforme).

Una volta completato il wizard, aggiungere la seconda interfaccia per la
rete interna, e la eventuale terza per il captive portal, ed assicurarsi
che sia queste che la prima interfaccia non abbiano spunta per l'uso del
firewall.

Inoltre, modificare il disco ed attivare la spunta di Discard.

Installare Debian
=================

Si esegua l'installazione in maniera ordinaria; per la rete si può anche usare
un eventuale DHCP, la configurazione scelta in fase di installazione verrà
comunque sovrascritta da quella che si imposterà in fase di deploy con
``cloud-init``.

Le operazioni specifiche che occorre fare in fase di installazione sono:

* si imposti per ``root`` la password di default: ``fuss``

* si usi un utente normale: ``fuss`` (o altro, andrà comunque cancellato)

* si configuri la rete nella maniera più semplice per accedere sulla macchina

* scegliere il partizionamento manuale del disco,

  * creare una prima partizione primaria di 4G come swap

  * creare una seconda partizione primaria con resto del disco come radice

* nella schermata *Selezione del software* scegliere solo *Server SSH* e
  *Utilità di sistema standard* (il resto si installerà dopo).


Preparazione dell'installazione base
====================================

Una volta completata l'installazione base si potrà passare a preparare il
server. Dato che l'installazione di default blocca l'accesso in SSH a root, o
si usa l'utente normale provvisorio creato in fase di installazione e si usa
``su`` o ``sudo``, o ci si collega sulla console via web. Tutte le operazioni
seguenti sono da eseguirsi dall'utente ``root``.

Il primo passo è abilitare da subito l'accesso di SSH a ``root``, inserendo
dentro ``/etc/ssh/sshd_config.d/root.conf``::

  # enable root password access
  PermitRootLogin yes

e riavviare ``sshd`` con ``service ssh restart``; non è necessario configurare
in questa fase l'uso di chiavi in ``authorized_keys``, queste possono essere
impostate in qualunque momento successivo tramite ``cloud-init``.

Si rimuova poi l'utente creato in fase di installazione con::

  # userdel -r fuss

Occorrerà anzitutto installare ``cloud-init`` ed alcuni programmi:

* ``gnupg`` per poter importare le chiavi di APT di FUSS;
* ``resolvconf`` per poter gestire la configurazione del DNS attraverso
  ``cloud-init``;
* ``bind9`` per poter configurare da subito la macchina in modalità
  compatibile con la configurazione che verrà impostata da
  ``fuss-server``
* ``quotatool`` per attivare le quote nel caso si aggiunga un disco
  separato per le home;

::

  # apt install cloud-init gnupg resolvconf bind9 quotatool

Per evitare i problemi di risoluzione con indirizzi IPv6 che si sono
verificati in alcuni casi, è opportuno che Bind sia configurato da subito per
usare solo IPv4, pertanto si modifichi in ``/etc/default/named`` l'ultima riga
in::

  OPTIONS="-4 -u bind"

e si riavvii Bind con ``systemctl restart named``.

Occorre poi configurare opportunamente ``/etc/apt/sources.list`` per usare i
repository di fuss, si utilizzi un contenuto analogo al seguente::

  # bookworm sources
  deb http://deb.debian.org/debian/ bookworm main
  deb-src http://deb.debian.org/debian/ bookworm main

  deb http://security.debian.org/debian-security bookworm-security main
  deb-src http://security.debian.org/debian-security bookworm-security main

  # fuss-sources
  deb [signed-by=/etc/apt/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main
  #deb [signed-by=/etc/apt/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm-proposed-updates main

(in genere basta aggiungere le ultime due righe), inoltre si deve importare la
chiave GPG di firma dei pacchetti, con::

  # wget -qO - https://archive.fuss.bz.it/apt.gpg | gpg --dearmor > /etc/apt/keyrings/fuss-keyring.gpg

Con il passaggio a Debian Buster e seguenti di default le interfacce di rete
assumono i nuovi nomi dipendenti dall'hardware, per cui al posto di ``eth0``
ed ``eth1`` si avranno nomi come ``ens18`` o ``ens19``. La configurazione
delle stesse con ``cloud-init`` riporta però automaticamente in uso i nomi
tradizionali, per cui non serve preoccuparsi di questo cambiamento nelle
configurazioni, con una eccezione: la eventuale terza interfaccia dedicata al
captive portal, che non deve essere configurata.

Non essendo configurata questa prenderà il nome di default, variando rispetto
a quello che si avrebbe con una Debian Jessie. Si può però evitare il
cambiamento (consentendo il riutilizzo senza cambiamenti dei nomi usati in un
precedente ``fuss-server.yaml``) configurando opportunamente le opzioni di
avvio del kernel, inserendo in ``/etc/default/grub`` la riga::

  GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0"

ed eseguendo ``update-grub``; in questo modo le interfacce verranno comunque
chiamate ``eth0``, ``eth1`` ed ``eth2``.

Infine per evitare che la voce di configurazione per ``lo`` presente in
``/etc/network/interfaces`` sovrascriva la stessa voce che ``cloud-init``
configura con un suo file sotto ``/etc/network/interfaces.d``
(``50-cloud-init.cfg``) si sposti la riga che include le configurazioni da
questa directory in fondo al file, in sostanza ``/etc/network/interfaces``
dovrà essere qualcosa del tipo::

  # This file describes the network interfaces available on your system
  # and how to activate them. For more information, see interfaces(5).

  # The loopback network interface
  auto lo
  iface lo inet loopback

  source /etc/network/interfaces.d/*

.. note:: si tenga presente che con questo ``/etc/network/interfaces`` non
          verrà configurata automaticamente nessuna interfaccia di rete, e se
          non si configura la stessa via ``cloud-init`` la macchina risulterà
          raggiungibile solo sulla console, pertanto se vi si deve accedere
          via rete per lavorarci sopra prima di creare l'immagine di
          ``cloud-init`` si lasci presente la configurazione dell'interfaccia
          che si intende utilizzare, rimuovendola nella fase di pulizia.

.. note:: si tenga presente che con questa configurazione si gestisce il
          contenuto di ``/etc/resolv.conf`` con ``cloud-init``, ma questo file
          viene anche scritto direttamente dal comando ``fuss-server``, per
          cui si abbia cura di impostare via  ``cloud-init`` lo stesso nome a
          dominio che si imposterà poi nel ``fuss-server`` e di indicare
          sempre come server DNS ``127.0.0.1``.

Infine occorre modificare ``/etc/aliases`` per rimuovere l'eventuale presenza
del reinoltro della posta dell'utente ``root`` all'utente di installazione
``fuss``; in sostanza va rimossa la riga::
  root: fuss

	  
Configurazione di ``cloud-init``
================================

La configurazione di default installata da ``cloud-init`` prevede la creazione
di un utente di default ed il blocco dell'accesso come ``root``. La scelta del
progetto è di fornire un accesso diretto a ``root`` a chiavi, con la
possibilità di usare una password, per questo vanno fatte alcune modifiche in
``/etc/cloud/cloud.cfg``. Anzitutto si devono cambiare le due righe
relative alla gestione degli utenti locali creati di default come nell'esempio
seguente::

  # A set of users which may be applied and/or used by various modules
  # when a 'default' entry is found it will reference the 'default_user'
  # from the distro configuration specified below
  users:
     - name: root

e poi bisogna evitare che venga applicata la configurazione che disabilita
l'uso dell'utente ``root``, configurando ``disable_root`` a ``false`` come
in::

  # If this is set, 'root' will not be able to ssh in and they
  # will get a message to login instead as the above $user (debian)
  disable_root: false

infine si aggiungano poi in coda al file la ulteriore configurazione che
consente a ``cloud-init`` di gestire ``/etc/hosts``::

  manage_etc_hosts: true

Si aggiunga poi sotto ``/etc/cloud/cloud.cfg.d/`` il file ``10_fuss.cfg``, con
il contenuto seguente (attenzione: le spaziature **non** devono contenere
tabulazioni)::

  apt:
      preserve_sources_list: true
  packages:
    - fuss-server

Pulizia
=======

Una volte effettuate le configurazioni precedenti si proceda a ripulire
l'immagine da tutti i dati spuri, ci si sconnetta e ci si riconnetta eseguendo
il comando::

  # set +o history

per disabilitare la history.

Se si è lasciato la configurazione dell'interfaccia di rete principale usata
in fase di installazione dentro ``/etc/network/interfaces`` la si rimuova.

Si eseguano poi (come root) i comandi::

  > .bash_history
  apt clean
  find /etc -name "*~" -delete
  journalctl --rotate
  journalctl --vacuum-time=1s
  fstrim /
  cd /var/log/
  > syslog
  > auth.log
  > cloud-init.log
  > cloud-init-output.log
  > debug
  > dpkg.log
  > messages
  > kern.log
  > user.log
  > daemon.log
  > installer/syslog
  > wtmp
  > btmp

per fare una pulizia finale, compreso ricreare vuoti i file modificati
dal sistema mentre era in uso.

Generazione dell'immagine
=========================

Una volte effettuate le configurazioni precedenti, si fermi la macchina
virtuale e se ne esegua un backup, chiedendo la compressione (coi valori
predefiniti in formato zst).  Si troverà il file del backup sotto
``/var/lib/vz/dump`` (o nella directory che si è configurato come
storage di backup) nella forma::

  vzdump-qemu-VMID-ANNO_ME_GI-OR_MI_SE.vma.zst

Pubblicazione
=============

Dopo eventuali test, caricare il file sulla macchina dove verrà
pubblicato, ad esempio nella home dell'utente root con il comando::

  $ scp /var/lib/vz/dump/vzdump-qemu-106-2023_06_05-16_06_42.vma.zst \
        root@iso.fuss.bz.it:

quindi collegarsi alla macchina stessa (``ssh root@iso.fuss.bz.it``) e
lanciare il comando::

  # cd /root
  # ./release_cloud_image.sh vzdump-qemu-106-2023_06_05-16_06_42.vma.zst

che provvede a spostare il file nella destinazione corretta e generarne
checksum e relative firme.

Quindi aggiornare il file ``/var/www/iso/cloud-init/changelog.txt`` con
l'elenco delle modifiche presenti nella release corrente.

L'immagine è ora disponibile per lo scaricamento su
https://iso.fuss.bz.it/cloud-init/

..  LocalWords:  cloud init FUSS Buster dump Proxmox fuss tech VM wizard wget
..  LocalWords:  portal netinstall CDROM storage local qcow DHCP deploy root
..  LocalWords:  swap SSH PermitRootLogin prohibit yes sshd service ssh keys
..  LocalWords:  restart authorized userdel gnupg APT resolvconf DNS bind apt
..  LocalWords:  install cfg This describes the interfaces available your and
..  LocalWords:  system how to activate them For information see loopback of
..  LocalWords:  interface iface inet source users which may applied used is
..  LocalWords:  various modules when entry found it will reference from If
..  LocalWords:  distro configuration specified below disable this not able
..  LocalWords:  they get message instead as above debian manage hosts true
..  LocalWords:  repository sources buster list deb main key BEGIN PGP PUBLIC
..  LocalWords:  BLOCK Version GnuPG proposed updates packages history clean
..  LocalWords:  bash find name delete syslog auth log debug dpkg messages ln
..  LocalWords:  kern daemon scp
