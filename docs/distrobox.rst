*********
Distrobox
*********

Premessa
-----------------------------------------------

In passato, per provare distribuzioni, applicazioni, simulare configurazioni con Linux, si doveva ricorrere alla virtualizzazione e alle macchine virtuali.
``distrobox`` apre alla possibilità di usare qualsiasi distribuzione Linux all’interno della stessa finestra del terminale.
Ciò è possibile grazie alla creazione di container Docker e Podman, inserendo al loro interno la distribuzione Linux di propria scelta.
Una soluzione come distrobox semplifica enormemente l’installazione e l’esecuzione di molteplici distribuzioni Linux direttamente dal terminale. Ciascun container creato risulta strettamente integrato con l’host, consentendo la condivisione della directory home dell’utente, l’accesso alle unità di memorizzazione esterne, l’utilizzo di dispositivi USB, delle applicazioni dotate di interfaccia grafica (X11/Wayland) e dell’hardware multimediale.
distrobox è un modo per integrare il software containerizzato come se fosse un’**app nativa** , anche se quel software appartiene a una distribuzione completamente diversa. 
Inoltre, permette di creare ambienti di sviluppo e di test senza richiedere i privilegi di root.

Come installare distrobox su Linux
----------------------------------

Distrobox è ormai presente nei repository delle varie distribuzioni, permettendo di caricare direttamente il pacchetto software. ::

    apt install distrobox

Una volta completata l’installazione di distrobox è possibile usare il comando seguente per caricare una distribuzione sotto forma di container.
In questo esempio creiamo una distribuzione Debian 10::

    distrobox-create --name debian10-box --image debian:10

Il nome può essere scelto in modo arbitrario e specificato dopo **--name**. Accanto a **--image**, invece, va specificato il nome della distribuzione Linux da installare.
Nella sezione `Containers Distros <https://github.com/89luca89/distrobox/blob/main/docs/compatibility.md#containers-distros>`_ della documentazione di distrobox
si trovano i nomi delle immagini utilizzabili nel comando **distrobox-create**.

Il comando che segue consente di accedere alla schermata del terminale corrispondente alla distribuzione Linux indicata::

    distrobox-enter --name debian10-box

Per chiudere il container si può semplicemente digitare ``exit + Invio`` dall'interno oppure da un altro terminale::

     distrobox-stop --name debian10-box

Installare applicazioni in distribuzioni distrobox
--------------------------------------------------

A questo punto siamo a tutti gli effetti nella distribuzione debian 10. Possiamo ad esempio installare nuove applicazioni.
Nel caso delle distribuzioni Debian e derivate (ad esempio Ubuntu), si può installare ovviamente in questo modo::

    sudo apt install omnitux

Abbiamo installato il programma di giochi Omnitux, che non è più compatibile con debian 12 ma lo è con debian10::

    sudo dpkg --add-architecture i386
    sudo apt install wine32

Abbiamo installato l'architettura **i386**, e l'emulatore **wine32** per poter utilizzare vecchi software senza dover modificare il sistema principale.
La cosa veramente interessante è che wine32, installato nel container, può essere utilizzato anche dal sistema principale (ad es. debian12),
per lanciare applicazioni contenute o nel file system del container o nella home dell'utente (che è condivisa).

È anche possibile eseguire comandi in una specifica distribuzione containerizzata senza acquisire prima l’accesso alla shell della distribuzione specificata
servendosi del comando che segue::

    distrobox-enter --name debian10-box -- sudo apt update

In questo caso il comando viene lanciato da debian 12 ma viene eseguito in debian 10.

Altri comandi utilizzabili con distrobox
----------------------------------------

Nella documentazione si trova l’ `elenco dei comandi  <https://github.com/89luca89/distrobox#what-it-does>`_ supportati da distrobox.
Tra i più utili c’è **distrobox-export**, uno strumento che consente di esportare un’applicazione dall’ambiente containerizzato e installarla sul sistema host. 
La sintassi è semplice. Se voglio esportare **dal container all'host** il programma omnitux installato prima::

    distrobox-export --app omnitux

Così facendo, l’applicazione esportata dal container apparirà automaticamente nella lista dei programmi disponibili sul “sistema ospitante”.
Se invece voglio lanciarlo nell'host dal terminale il comando è::

    /usr/bin/distrobox-enter -n debian10-box -- omnitux

Si è scelto intenzionalmente l'esempio di Omnitux perchè richiede qualche configurazione aggiuntiva che in genere non è necessaria.
Bisogna innanzitutto riconfigurare la lingua di sistema. ::

    apt install locales

    dpkg-reconfigure locales   # Aggiungere it_IT.UTF-8 e sceglierlo come default

Nel lanciatore  **/usr/bin/omnitux** aggiungere la riga::

    export LC_ALL=it_IT.UTF-8

Può essere utile anche la possibilità di **clonare** un container, per destinare i cloni ad altro uso o fare un backup.
Lo si può fare rapidamente con Podman rilevando dapprima l’ID corrispondente (podman ps),
arrestando il container (podman stop ID_container) quindi impartendo il comando seguente::

    distrobox-create --name debian-10-clonato --clone debian10-box

Nell’esempio, il contenuto del container debian10-box viene automaticamente clonato nel nuovo container di nome debian-10-clonato.

Come salvare e reimportare i container
--------------------------------------

Per salvare, esportare e riusare un container già configurato esiste anche la possibilità di creare uno **snapshot**
e di reimportarlo in un altro host (con qualche limitazione, come vedremo)

Per salvare un’immagine con podman::

    podman container commit -p distrobox_name  backup_image_name  # Il nome del backup lo scegliamo noi

::

    podman image save backup_image_name:latest | bzip2 > backup_image_name.tar.bz2

Questo creerà un archivio compresso tar.gz del contenitore scelto.

Ora lo si può trasferire su un altro host e per ripristinarlo basta eseguire::

    podman image load -i  backup_image_name.tar.bz2

E creare un nuovo contenitore basato su quell'immagine::

    distrobox-create --image backup_image_name:latest --name distrobox_name


Limitazioni nell'esportazione ed importazione di container
----------------------------------------------------------

Per quello che si è potuto testare, non è al momento possibile caricare un'immagine
nella home di un'utente di rete, ma solo in quelle di utenti locali. Problemi di permessi legati al montaggio
delle home via nfs impediscono il caricamento del container podman.
Per avere la stessa shell (ad esempio bash) dell'utente, si può avviare distrobox col comando::

    distrobox-enter --name debian10-box -- bash

Per cambiare la shell in modo permanente per l'utente, si può sostituire **/bin/sh** con **/bin/bash** nel file **/etc/passwd** .
Questo sostituzione si può effettuare anche col comando::

    distrobox-enter --name debian10-box -- sudo sed -i "/my_username/s|/bin/sh|/bin/bash|" /etc/passwd

Esempio di applicazione di distrobox nel progetto Fuss
------------------------------------------------------

Come già accennato, il fatto di poter utilizzare una distro linux all'interno di un'altra apre la strada alla possibilità
di integrare nel sistema principale applicazioni non compatibili (in quanto obsolete, o richiedenti librerie o architetture particolari)
senza alterare il sistema stesso.
Ad esempio, nel pacchetto ``fuss-distrobox`` per fuss 11 e fuss 12 viene scaricato via wget un container debian 10
con alcune applicazioni non più supportate. Il pacchetto crea un utente (gioco), vi scarica l'immagine debian 10,
copia varie altre cartelle e modifica i permessi attraverso gli script,
in modo tale che l'utente utilizza tali applicazioni senza nemmeno accorgersi dell'esistenza del container podman.

