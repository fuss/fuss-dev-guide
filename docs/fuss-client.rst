***********
FUSS Client
***********

``fuss-client`` è uno script python che lancia un playbook ansible_ che
configura una macchina come client in una rete FUSS.

.. _ansible: https://docs.ansible.com/

fuss-client
===========

Lo script ``fuss-client`` è scritto per python 3.

Le opzioni ``-a | -U | -r | -l`` sono mutualmente esclusive e
corrispondono rispettivamente ai metodi ``add``, ``upgrade``, ``remove``
e ``listavail``; ad eccezione di quest'ultimo si concludono con
l'``os.execvp`` di un comando di shell per lanciare ansible; notare che
questo termina l'esecuzione del programma python, eventuale codice
successivo non viene eseguito.

Prima della configurazione, l'opzione ``-a`` ricerca e contatta un
fuss-server (metodi ``_test_connection`` e ``_get_cluster``) per
aggiungere la macchina corrente ad un cluster, tramite l'api di
octofuss.

Notare che non esiste un'api corrispondente per rimuovere una macchina
da un cluster, operazione che va svolta lato server.

Il passo successivo è la generazione di una chiave kerberos per il
client: questa operazione viene svolta sul server dallo script
``add_client_principal``, richiamato tramite ssh, quindi la chiave viene
copiata localmente tramite scp.
Per l'autenticazione sul server, sono supportati vari casi: accesso come
root, accesso come utente con permessi sudo, oppure accesso con chiave
con permessi limitati alle sole operazioni necessarie per lo script.

Playbook
========

Ansible viene chiamato con uno dei seguenti playbook, a seconda del
sottocomando usato:

``connect.yml``
   per configurare un fuss-client
``remove.yml``
   per eliminare la configurazione del fuss-client.

Quest'ultimo ripristina alcuni file di configurazione dai backup, il
primo non compie direttamente azioni, ma richiama ruoli dalla
directory ``roles``.

Compatibilità raspbian
----------------------

Alcuni task, ed in particolare quelli relativi a lightdm, non vanno
eseguiti quando la distribuzione base non è fuss-client (o una normale
Debian), ma Raspbian, che richiede alcune personalizzazioni specifiche;
per questi si usa la condizione ``when: ansible_lsb.id != "Raspbian"``.

Pacchetti Debian
================

Il repository prevede la generazione di due pacchetti .deb,
``fuss-client`` e ``fuss-client-dependencies``; il primo contiene il
fuss-client vero e proprio, mentre il secondo è un metapacchetto che
dipende da tutti i pacchetti installati dal playbook ansible.

``fuss-client-dependencies`` non è necessario per l'uso di fuss-client,
ma è aggiunto per comodità per pre-installare (e soprattutto
pre-scaricare) tutti i pacchetti necessari.

Numeri di versione
------------------

Il pacchetto ``fuss-client`` è nativo, quindi il numero di versione è
del tipo X.Y.Z dove X è il numero di versione debian corrispondente (ad
esempio 8 per jessie, 9 per stretch, 10 per buster).

HOWTO
=====

Script all'avvio
----------------

Per installare su fuss-client degli script che vengano eseguiti
all'avvio il metodo raccomantato è di usare delle unit systemd.

Per farlo, installare lo script desiderato in ``/usr/local/bin`` (o
``sbin``, se ha senso che venga eseguito solo da ``root``), ad esempio
come ``/usr/local/bin/my_script.sh`` con permessi di esecuzione, quindi
creare il file ``/etc/systemd/system/my-script.service`` con i seguenti
contenuti::

   [Unit]
   Description=My script doing things
   After=network.target

   [Service]
   ExecStart=/usr/local/bin/my_script.sh

   [Install]
   WantedBy=multi-user.target

ed abilitare la unit.

In ansible, serviranno dei task tipo i seguenti::

   - name: Script to do things
     copy:
         dest: /usr/local/bin/my_script.sh
         src: my_script.sh
         mode: 0755
   - name: Do things at startup
     copy:
         dest: /etc/systemd/system/my-script.service
         src: my-script.service
   - name: Enable doing things at startup
     systemd:
         enabled: yes
         name: do-things
