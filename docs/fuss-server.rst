***********
FUSS Server
***********

``fuss-server`` è uno script python che lancia un playbook ansible_ che
configura una macchina per poter funzionare come server in una rete
FUSS.

.. _ansible: https://docs.ansible.com/

fuss-server
===========

Lo script ``fuss-server`` è scritto per essere compatibile con python 2
e 3; nel momento in cui ansible passerà ad usare python 3 si potrà
eliminare la compatibilità python 2.

I vari sottocomandi corrispondono alle funzioni con lo stesso nome e
generalmente si concludono con l'``os.execvp`` di un comando di shell
per lanciare ansible; notare che questo termina l'esecuzione del
programma python, eventuale codice successivo non viene eseguito.

Playbook
========

Ansible viene chiamato con uno dei seguenti playbook, a seconda del
sottocomando usato:

``create.yml``
   per configurare da zero un fuss-server.
``upgrade.yml``
   per aggiornare la configurazione di un fuss-server.
``captive_portal.yml``
   per applicare la configurazione aggiuntiva necessaria sui captive
   portal.
``purge.yml``
   per eliminare la configurazione del fuss-server.

Quest'ultimo ripristina alcuni file di configurazione dai backup, gli
altri non compiono direttamente azioni, ma richiamano ruoli dalla
directory ``roles``, in modo da poter condividere il codice, in
particolare tra ``create`` e ``upgrade``.

Pacchetti Debian
================

Il repository prevede la generazione di due pacchetti .deb,
``fuss-server`` e ``fuss-server-dependencies``; il primo contiene il
fuss-server vero e proprio, mentre il secondo è un metapacchetto che
dipende da tutti i pacchetti installati dal playbook ansible.

``fuss-server-dependencies`` non è necessario per l'uso di fuss-server,
ma è aggiunto per comodità per pre-installare (e soprattutto
pre-scaricare) tutti i pacchetti necessari.

Per le istruzioni su come buildare i pacchetti e caricarli su
archive.fuss.bz.it si può vedere l'articolo :doc:`pacchetti-e-repository`

Numeri di versione
------------------

Il pacchetto ``fuss-server`` è nativo, quindi il numero di versione è
del tipo X.Y.Z dove X è il numero di versione debian corrispondente (ad
esempio 8 per jessie, 9 per stretch, 10 per buster).

Dipendenze
----------

Alcune delle dipendenze del pacchetto fuss-server, in particolare
ansible (nella versione richiesta) e python-ruamel.yaml sono disponibili
solo in ``jessie-backports``; per installare il pacchetto è necessario
aver abilitato quel repository, e per usarlo è necessario avere anche il
repository di FUSS.
