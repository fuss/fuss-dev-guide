***********************
Guide del progetto FUSS
***********************

Le guide del progetto fuss (user_, referent_, tech_ e dev_) sono scritte
in reStructuredText_ e pubblicate usando il generatore di documentazione
sphinx_

.. _user: https://readthedocs.org/projects/fuss-user-guide/
.. _referent: https://readthedocs.org/projects/fuss-referent-guide/
.. _tech: https://readthedocs.org/projects/fuss-tech-guide/
.. _dev: https://readthedocs.org/projects/fuss-dev-guide/
.. _reStructuredText: http://www.sphinx-doc.org/en/stable/rest.html
.. _sphinx: http://sphinx-doc.org/

Repository
==========

I sorgenti delle guide sono in repository git hostati su gitlab; avendo
un'account sulla piattaforma ed una `chiave ssh configurata`_ possono
essere clonati con::

    git clone git@gitlab.com:fusslab/fuss-user-guide.git
    git clone git@gitlab.com:fusslab/fuss-referent-guide.git
    git clone git@gitlab.com:fusslab/fuss-tech-guide.git
    git clone git@gitlab.com:fusslab/fuss-dev-guide.git

altrimenti si può usare l'accesso https::

    git clone https://gitlab.com/fusslab/fuss-user-guide.git
    git clone https://gitlab.com/fusslab/fuss-referent-guide.git
    git clone https://gitlab.com/fusslab/fuss-tech-guide.git
    git clone https://gitlab.com/fusslab/fuss-dev-guide.git

.. _`chiave ssh configurata`: https://gitlab.com/help/ssh/README.md

Creazione di nuove pagine
=========================

Per aggiungere una pagina ad una guida:

* Creare un nuovo file con estensione ``.rst``, ad esempio
  ``titolo-dell-articolo.rst``; è opportuno che il nome del file
  contenga solo lettere minuscole, numeri e il trattino ``-``, senza
  lettere accentate, spazi né altri caratteri speciali.

* Aggiungere l'articolo al *doctree* in ``index.rst``, aggiungendo il
  nome del file senza estensione (es. ``titolo-dell-articolo``) nella
  posizione opportuna rispetto agli altri articoli.

Build locale
============

Mentre si stanno facendo modifiche, può essere utile generare localmente
le pagine html per avere un'anteprima di quanto sarà poi pubblicato.

Setup
-----

Per la generazione è necessario installare le seguenti dipendenze (su
FUSS client, o una versione recente di Debian o derivate)::

    apt install python3-sphinx python3-sphinx-rtd-theme python3-stemmer

Build
-----

Per generare le pagine html::

    cd <path/del/repository>/docs
    make html


e si può poi vedere il risultato con::

    sensible-browser _build/html/index.html

Aggiornamento versione pubblicata
=================================

La versione pubblicata su readthedocs viene aggiornata automaticamente
ogni volta che viene aggiornato il branch ``master`` su gitlab; se si ha
accesso in scrittura ai repository è sufficiente::

    cd <path/del/repository>
    git push origin master

(oppure semplicemente ``git push`` se ci si trova già sul branch
``master``)

Se non si ha accesso in scrittura al repository si può invece creare una
`merge request`_

.. _`merge request`: https://gitlab.com/help/user/project/merge_requests/index.md

Linee di guida per lo stile
===========================

Screenshot ed esempi di terminale
---------------------------------

Per guide che si riferiscono a programmi grafici, è opportuno aggiungere
screenshot.

Per quanto riguarda esempi tratti dal terminale, meglio riportare
comandi ed output come testo, per maggiore accessibilità, usando dei
blocchi di codice, ad esempio::

    testo introduttivo::

        $ comando
        output del comando
        # /sbin/comando
        output del comando lanciato da root

.. note:: I doppi due punti del codice reStructuredText vengono
  convertiti in un punto unico nelle versioni pubblicate, a meno che non
  sia preceduto da spazi (o in un paragrafo da solo), nel qual caso
  viene rimosso.

Livelli di struttura
--------------------

reStructuredText permette di usare caratteri diversi per i vari livelli
di struttura di un documento, purché siano coerenti all'interno del
singolo file.

È però meglio attenersi alla convenzione della `Python’s Style Guide for
documenting <https://devguide.python.org/documenting/#sections>`_, che
prevede:

* ``#`` con doppia riga per le parti (non usate in questi manuali);
* ``*`` con doppia riga per i capitoli (corrisponenti per noi ai file);
* ``=`` per le sezioni;
* ``-`` per le sottosezioni;
* ``^`` per le sotto-sottosezioni;
* ``"`` per i paragrafi.

Tips e tricks
=============

Migrazione dalla wiki di Redmine
--------------------------------

I documenti presenti sulla wiki di redmine usano il formato (sorgente)
textile; per convertirlo in reStructuredText può essere utile il
programma `Pandoc <https://pandoc.org/>`_

Una volta copiato il sorgente della pagina in un file locale
``articolo.txt`` si può usare il seguente comando per ottenerne una
versione in reStructuredText nel file ``articolo.rst``::

   pandoc -f textile -t rst -o articolo.rst articolo.txt

Tale file è un buon punto di partenza, ma non è pronto per l'inclusione
diretta nelle guide FUSS senza prima verificare manualmente i contenuti;
in particolare sarà sicuramente necessario sistemare manualmente alcune
caratteristiche.

* I livelli di struttura andranno corretti per adeguarsi allo standard
  specificato qui sopra, facendo attenzione alla posizione in cui viene
  inserito il documento (ad esempio se si sta convertendo una pagina
  della wiki in una sezione o sottosezione di una pagina della guida).

* Le immagini vengono caricate dal file originale sulla wiki; per la
  mantenibilità futura è invece opportuno caricarle localmente
  all'interno della guida.

  Dopo aver scaricato le immagini, ad esempio nella directory
  ``images/articolo/``, e spostati i riferimenti generati da pandoc
  dalla fine del documento alla posizione dove dovrà apparire
  l'immagine, li si può convertire in direttive ``figure`` col seguente
  comando di vim::

     :%s/|image.*| image:: https:\/\/work.fuss.bz.it\/attachments\/download\/.*\//figure:: images\/articolo\//

* pandoc genera dei blocchi di codice introdotti da una riga contenente
  solo ``::``, anche quando il paragrafo precedente termina con ``:``;
  per migliore eleganza e leggibilità del sorgente questi si possono
  convertire mettendo ``::`` solo sul paragrafo precedente.

  Entrambi i casi sono comunque costruzioni reST valide che vengono
  compilate in una presentazione simile.

* Alcuni articoli della wiki contengono sezioni con indicazioni tipo
  “attenzione” o “nota”: in questi casi può valere la pena convertirli
  nella relativa `direttiva specifica reStructuredText
  <http://docutils.sourceforge.net/0.4/docs/ref/rst/directives.html#specific-admonitions>`_

Screenshot
----------

Redimensionare una finestra con precisione
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Per fare screenshot della finestra di un programma è utile
ridimensionarla alla dimensione precisa che si vuole dare allo
screenshot; per farlo si può usare il comando seguente::

   sleep 3 ; xdotool getactivewindow windowsize 1024 768

che aspetta 3 secondi, nel corso del quale si può cambiare la finestra
attiva dal terminale alla finestra desiderata, e quindi effettua il
resize.
