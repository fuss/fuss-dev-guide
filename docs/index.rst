FUSS - Manuale per lo sviluppatore
==================================

.. figure:: images/fuss.png
      :width: 100px
      :align: right

Il presente manuale è una guida alla manutenzione ed allo sviluppo della distribuzione `FUSS <https://fuss.bz.it>`_ GNU/Linux basata, lato server e client su Debian 12 "Bookworm".

La versione più recente di questo manuale è leggibile all'indirizzo
https://fuss-dev-guide.readthedocs.io/it/latest/

.. toctree::
   :maxdepth: 2

   guide-progetto-fuss
   pacchetti-e-repository
   appunti-su-git
   metapacchetti
   fuss-server
   fuss-client
   cloud-init-image
   iso
   nuova-versione-debian
   macchine-virtuali-libvirt-qemu-kvm
   distrobox


Contribuisci
""""""""""""

Chiunque può contribuire a migliorare questa documentazione che è scritta in `reStructuredText <http://www.sphinx-doc.org/rest.html>`_.

Supporto
""""""""

Se ti serve aiuto, scrivi una mail ad info@fuss.bz.it

Licenze
"""""""

.. image:: https://img.shields.io/badge/code-GPLv3-blue.png
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html
    :alt: Code GPLv3

.. image:: https://img.shields.io/badge/documentation-CC%20BY--SA-lightgrey.png
    :target: https://creativecommons.org/licenses/by-sa/4.0/
    :alt: Documentation CC BY-SA
