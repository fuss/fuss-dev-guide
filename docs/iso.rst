**************
ISO FUSS 12
**************

Con FUSS 12, il tool adottato per personalizzare una immagine ISO rimane ``remaster-iso`` (https://github.com/unixabg/remaster-iso).

Creare la ISO live di FUSS
==========================

Fino a  con Debian 10 era necessario clonare il progetto ``remaster-iso`` 
da GitHub e spostarsi nella cartella creata.
A partire da Debian 11 "bullseye" ``remaster-iso`` è disponibile come pacchetto .
dopo l'installazione viene creata la cartella ``/usr/share/remaster-iso`` che contiene il file ``remaster-iso.conf``

Di seguito viene mostrato come personalizzare l'immagine di Debian Live amd64 Xfce  


Creare una cartella di produzione della ISO e copiarvi la ISO con ::

    wget https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/debian-live-12.8.0-amd64-xfce.iso

.. note::

    È importante creare la ISO fuss entro poco tempo dal rilascio  
    della ISO debian Live per evitare problemi di installazione di
    versioni di grub-pc o grub-efi-amd64 non disponibili installando
    in assenza di connessioni di rete.
    
ed il file di configurazione ``remaster-iso.conf`` , 
da modificare analogamente all'esempio che segue ::

    ######################
    ## remaster settings
    ######################
    _BASEDIR=$(pwd)
    _ISOExtractPath="${_BASEDIR}/iso-extract"
    _ISOLivePath="live"
    _ISOMountPoint="${_BASEDIR}/iso-mount"
    _ISOName=""
    _ISOTargetName="fuss-12-amd64-live-lite"
    _ISOTargetTitle="FUSS 12 amd64 live lite"

    _VER="0.9.4"

.. note::

    I comandi remaster possono essere lanciati come file di sistema con ::
    
    - remaster-extract
    - remaster-squashfs-editor
    - remaster-compose

    ma di seguito vengono lanciati come se fossero scaricati da GitHub


Estrarre il file .iso ::

    ./remaster-extract -i debian-live-12.1.0-amd64-xfce.iso

Lanciare poi il comando ``remaster-squashfs-editor`` e selezionare "C"
premendo INVIO::

    ./remaster-squashfs-editor

::

    #################################
        remaster-squashfs-editor
        remaster-iso version 0.9.4
    #################################
      (C)hroot - Chroot in to the filesystem.squashfs + psu-*.squashfs stack.
      (J)oin   - Join the partial squashfs update files to new single psu-DATE.squashfs
      (N)ew    - New master filesystem.squashfs file which joins all *.squashfs file to new single filesystem.squashfs
      E(x)it   - Exit the program with no action
    Enter choice [C , J , N , X] C

Modificare il file dei repository se necessario ::

    nano /etc/apt/sources.list

::

    # ##### Debian Main Repos
    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian bookworm main 
    deb-src http://deb.debian.org/debian bookworm main 

    deb http://deb.debian.org/debian bookworm-updates main 
    deb-src http://deb.debian.org/debian bookworm-updates main 

    deb http://security.debian.org/debian-security/ bookworm-security main 
    deb-src http://security.debian.org/debian-security/ bookworm-security main 

::

    nano /etc/apt/sources.list.d/deb_debian_org_debian.list

::

    # ##### Debian Backports Repo
    deb http://httpredir.debian.org/debian bookworm-backports main 
    
::

    nano /etc/apt/sources.list.d/archive_fuss_bz_it.list

::

    # ##### FUSS Main Repo
    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg]  http://archive.fuss.bz.it/ bookworm main 
    deb-src [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main 



Se servono anche pacchetti contrib e non-free, il file devono essere modificati come segue::

    nano /etc/apt/sources.list

::

    # ##### Debian Main Repos
    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian bookworm main contrib non-free-firmware non-free
    deb-src http://deb.debian.org/debian bookworm main contrib non-free-firmware non-free

    deb http://deb.debian.org/debian bookworm-updates main contrib non-free-firmware non-free
    deb-src http://deb.debian.org/debian bookworm-updates main contrib non-free-firmware non-free

    deb http://security.debian.org/debian-security/ bookworm-security main contrib non-free-firmware non-free
    deb-src http://security.debian.org/debian-security/ bookworm-security main contrib non-free-firmware non-free

::

    nano /etc/apt/sources.list.d/deb_debian_org_debian.list

::

    # ##### Debian Backports Repo
    deb http://httpredir.debian.org/debian bookworm-backports main contrib non-free-firmware non-free
    
::

    nano /etc/apt/sources.list.d/archive_fuss_bz_it.list

::

    # ##### FUSS Main Repo
    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg]  http://archive.fuss.bz.it/ bookworm main contrib non-free
    deb-src [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main contrib non-free

Aggiornare i pacchetti ::

    apt update
    apt install curl wget apt-transport-https dirmngr
    wget -qO - https://archive.fuss.bz.it/apt.key | gpg --dearmour > /usr/share/keyrings/fuss-keyring.gpg
    apt update && apt full-upgrade

Installare ``fuss-client`` ::

    apt install fuss-client

Lanciare il comando per la configurazione di FUSS standalone (desktop) ::

    fuss-client --standalone --light [--unofficial] [--locale=LOCALE] --domain fuss.bz.it

dove

- ``--light`` mantiene l'immagine leggera senza installare i
  metapacchetti didattici;
- ``--unofficial`` permette di installare i firmware non-free di debian;
- ``--locale=LOCALE`` permette di scegliere la lingua di default, dove
  ``LOCALE`` è, a titolo d'esempio, nella forma de\_DE.UTF-8.

Nello specifico:

- per la **ISO FUSS ufficiale lite**::

    fuss-client --standalone --light --domain fuss.bz.it

- per la **ISO FUSS ufficiale full**::

    fuss-client --standalone --domain fuss.bz.it

- per la **ISO FUSS "unofficial" light**::

    fuss-client --standalone --light --unofficial --domain fuss.bz.it

- per la **ISO "unofficial" full**::

    fuss-client --standalone --unofficial --domain fuss.bz.it

Se non fatto precedentemente, scaricare la chiave del repository
``archive.fuss.bz.it`` che verrà utilizzata da Calamares
(https://calamares.io/) durante l'installazione::

    curl https://archive.fuss.bz.it/apt.key | gpg --dearmor > /usr/share/keyrings/fuss-keyring.gpg

.. note::

    Se il fuss-client --standalone ... dovesse fallire con l'errore:
    
    **ERROR: Ansible could not initialize the preferred locale: unsupported locale setting**
    
    riconfigurare i locales scegliendo una lingua lanciando il comando:
        
    ``dpkg-reconfigure locales``

Modificare lo script ``/usr/sbin/sources-final`` che scriverà i
repository durante l'installazione::

    #!/bin/sh
    #
    # Writes the final sources.list files
    #

    CHROOT=$(mount | grep proc | grep calamares | awk '{print $3}' | sed -e "s#/proc##g")
    RELEASE="bookworm"
    FUSS_KEY="/usr/share/keyrings/fuss-keyring.gpg"

    cat << EOF > $CHROOT/etc/apt/sources.list
    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian $RELEASE main
    deb-src http://deb.debian.org/debian $RELEASE main

    deb http://deb.debian.org/debian $RELEASE-updates main
    deb-src http://deb.debian.org/debian $RELEASE-updates main

    deb http://security.debian.org/debian-security/ $RELEASE-security main
    deb-src http://security.debian.org/debian-security/ $RELEASE-security main
    EOF

    cat << EOF > $CHROOT/etc/apt/sources.list.d/deb_debian_org_debian.list
    deb http://deb.debian.org/debian $RELEASE-backports main
    deb-src http://deb.debian.org/debian $RELEASE-backports main
    EOF

    cat << EOF > $CHROOT/etc/apt/sources.list.d/archive_fuss_bz_it.list
    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ $RELEASE main
    deb-src [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ $RELEASE main
    EOF

    if [ -f $FUSS_KEY ] ; then
        cp $FUSS_KEY $CHROOT/usr/share/keyrings/fuss-keyring.gpg
    fi

    exit 0

Per le immagini unofficial il file dev'essere ::

    #!/bin/sh
    #
    # Writes the final sources.list files
    #

    CHROOT=$(mount | grep proc | grep calamares | awk '{print $3}' | sed -e "s#/proc##g")
    RELEASE="bookworm"
    FUSS_KEY="/usr/share/keyrings/fuss-keyring.gpg"

    cat << EOF > $CHROOT/etc/apt/sources.list
    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian $RELEASE main contrib non-free-firmware non-free
    deb-src http://deb.debian.org/debian $RELEASE main contrib non-free-firmware non-free

    deb http://deb.debian.org/debian $RELEASE-updates main contrib non-free-firmware non-free
    deb-src http://deb.debian.org/debian $RELEASE-updates main contrib non-free-firmware non-free

    deb http://security.debian.org/debian-security/ $RELEASE-security main contrib non-free-firmware non-free
    deb-src http://security.debian.org/debian-security/ $RELEASE-security main contrib non-free-firmware non-free
    EOF

    cat << EOF > $CHROOT/etc/apt/sources.list.d/deb_debian_org_debian.list
    deb http://deb.debian.org/debian $RELEASE-backports main contrib non-free
    deb-src http://deb.debian.org/debian $RELEASE-backports main contrib non-free
    EOF

    cat << EOF > $CHROOT/etc/apt/sources.list.d/archive_fuss_bz_it.list
    deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ $RELEASE main contrib non-free
    deb-src [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ $RELEASE main contrib non-free
    EOF

    if [ -f $FUSS_KEY ] ; then
        cp $FUSS_KEY $CHROOT/usr/share/keyrings/fuss-keyring.gpg
    fi

    exit 0

Sostituire il file ``main.py`` nella cartella
``/usr/lib/x86_64-linux-gnu/calamares/modules/displaymanager`` con la
versione modificata disponibile al link
https://www.fuss.bz.it/utility/calamares/main.py ::

    wget -O /usr/lib/x86_64-linux-gnu/calamares/modules/displaymanager/main.py https://www.fuss.bz.it/utility/calamares/main.py

.. note::

    Verificare periodicamente che il modulo main.py sia ancora compatibile con l'ultima versione di ``calamares``.
    Eventualmente modificarlo.

Rimuovere i pacchetti **uim**, **ibus** e, se necessario, riconfigurare **libdvd-pkg** ::

    apt remove uim ibus
    dpkg-reconfigure libdvd-pkg

Rimuovere i pacchetti inutilizzati e ripulire la cache pacchetti ::

    apt-get autoremove
    apt-get clean

Configurare la live ::

    nano /etc/live/config.conf.d/fuss.conf

::

    LIVE_HOSTNAME="fuss"
    LIVE_USERNAME="user"
    LIVE_USER_FULLNAME="FUSS Live user"
    LIVE_LOCALES="en_US.UTF-8,it_IT.UTF-8,de_AT.UTF-8"
    LIVE_TIMEZONE="Europe/Rome"
    LIVE_KEYBOARD_LAYOUTS="it,de"

Cambiare l'encoding della console ::

   nano /etc/default/console-setup

impostando ::

   CHARMAP="UTF-8"

Cambiare l'hostname di default ::

    nano /etc/hostname

::

    fuss

Per prendere le impostazioni del pannello come previsto da FUSS,
modificare una riga dello script ``/lib/live/config/1170-xfce4-panel``::

    nano /lib/live/config/1170-xfce4-panel

::

    sudo -u "${LIVE_USERNAME}" cp /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml /home/"${LIVE_USERNAME}"/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

Qualora servisse, nella cartella ``/lib/live/config`` ci sono tutti gli
script richiamati dalla live per le diverse configurazioni. Come
documentazione c'è la man page di ``live-config`` dov'è tutto abbastanza ben
documentato.

Per evitare che all'avvio della ISO live appaia lo sfondo artwork presente al momento della compilazione della stessa,
si consiglia di modificare lo script ``/usr/share/fuss-artwork/rotate-background.sh``, aggiungendo: ::

    if grep -w "overlay" /etc/fstab; then
        EXPECTED_LINK="/usr/share/fuss-artwork/wallpapers/fuss-12-wallpaper.svg"
    fi

subito prima della riga: ::

    test "$CURRENT_LINK" = "$EXPECTED_LINK" && exit 0

In questo modo, in modalità Live apparirà lo sfondo predefinito per la versione installata.


Terminate le modifiche fatte in chroot, uscire dall'Editor di squashfs e salvare digitando Y ed invio ::

    root@jarvis:~# exit
    Exited the chroot so time to clean up.
    Restore original overlay/etc/hosts.
    Restore overlay/etc/resolv.conf.
    Remove overlay/root/.bash_history.
    #############################################################
     (Y)es save my chroot modifications.
     (N)o do not save my chroot modifications.
    Select to save your chroot modifications (default is N):

    Y
    Now making the updated squashfs 20230614-013407.
    Parallel mksquashfs: Using 8 processors
    Creating 4.0 filesystem on psu-20230614-013407.squashfs, block size 131072.

Lanciare nuovamente ``./remaster-squashfs-editor`` e scegliere l'opzione ``N``
confermando poi con ``Y`` la creazione di ``filesystem.squashfs``::

    ./remaster-squashfs-editor

::

    #################################
        remaster-squashfs-editor
        remaster-iso version 0.9.4
    #################################
      (C)hroot - Chroot in to the filesystem.squashfs + psu-*.squashfs stack.
      (J)oin   - Join the partial squashfs update files to new single psu-DATE.squashfs
      (N)ew    - New master filesystem.squashfs file which joins all *.squashfs file to new single filesystem.squashfs
      E(x)it   - Exit the program with no action
    Enter choice [C , J , N , X] N
    I: New option selected!
    I: change directory to target live folder
    I: strting mount list and points operations
    I: found ./psu-20230614-020636.squashfs ... setting up mount point of psu-20230614-020636_squashfs
    I: mounting ./psu-20230614-020636.squashfs on psu-20230614-020636_squashfs
    ./psu-20230614-020636_squashfs:./filesystem_squashfs
    ./psu-20230614-020636_squashfs ./filesystem_squashfs ./psu_overlay
    ./psu-20230614-020636_squashfs:./filesystem_squashfs
    #############################################################
     (Y)es, create a new single filesystem.squashfs.
     (N)o, do not create a new single filesystem.squashfs.
    Select to create a new single filesystem.squashfs (default is N):

    Y

Rimuovere la cartella ``./iso-extract/live/psu-OOS*`` ::

    rm -fr ./iso-extract/live/psu-OOS*

Copiare i file kernel-related presenti nello squashfs nella cartella
``./iso-extract/live`` ::

    config-6.1.0-21-amd64
    initrd.img-6.1.0-21-amd64
    System.map-6.1.0-21-amd64
    vmlinuz-6.1.0-21-amd64
   
Per fare questo lanciare nuovamente ``./remaster-squashfs-editor`` scegliendo 
l'opzione ``(C)hroot``
Aprire un altro terminale e spostarsi in iso-extract/live.
Copiarvi i file da iso-extract/live/psu_overlay/boot/, dove è stata montata la cartella, eliminando eventualmente quelli con la versioni non desiderate ::

    cp /root/iso/fuss-12-amd64-.../iso-extract/live/psu_overlay/boot/* .


Si esca dall'ambiente chroot senza apportare modifiche.

Si modifichi all'occorrenza l'immagine ``iso-extract/isolinux/splash.png`` 
ed eventualmente anche ``iso-extract/isolinux/splash800x600.png``.

Modificare i file per personalizzare il menu di boot a piacimento. Si può decidere ad esempio di mantenere solo l'opzione live (che include
l'installer Calamares) e di rimuovere le opzioni ``Graphical DebianInstaller``, 
``Debian Installer`` e 
``Debian Installer with Speech Synthesis``::

    iso-extract/isolinux/menu.cfg
    iso-extract/boot/grub/grub.cfg

.. note::

    È importante ricordarsi di modificare i due file precedenti nel caso 
    di aggiornamenti del kernel, in modo che le versioni corrispondano.
    Nel caso nel menu di boot manchino alcune opzioni, come il supporto di localizzazione per avviare la live nella lingua desiderata, 
    si possono copiare parti di codice dai file di distribuzioni precedenti che avevano tale supporto.


È arrivato il momento di generare la nuova ISO lanciando il comando ::

    ./remaster-compose

Al termine dello script si troverà nell'attuale cartella di lavoro la
nuova immagine .iso.

.. note::

    Per successivi aggiornamenti e personalizzazioni, sarà sufficiente
    partire dall'immagine ISO creata precedentemente facendo solo le
    modifiche necessarie ed utilizzando i tre script di
    ``remaster-iso`` ed eventualmente aggiornando i file menu.cfg e
    grub.cfg come indicato sopra.

L'immagine .iso generata andrà copiata sul server ``iso.fuss.bz.it``
nella cartella di pubblicazione ``[..]/fuss12/client/``, raggiungibile
via web al link https://iso.fuss.bz.it/fuss12/client/ .

Nella stessa cartella dev'essere aggiornato il file
``changelog-live-iso.txt`` con le modifiche presenti nella nuova
versione pubblicata.

Infine vanno rigenerati i checksum ``sha256sum`` firmando a seguire il
file ``sha256sums.txt`` con i seguenti comandi o con lo script
``iso-sha256sums.sh`` di cui root dispone ::

    rm -f sha256sums.txt sha256sums.txt.asc
    sha256sum fuss-12-* > sha256sums.txt && gpg --armour --detach-sign sha256sums.txt
