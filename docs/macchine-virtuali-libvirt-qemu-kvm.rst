********************************************************************
Macchine virtuali con libvirt+qemu/kvm per fuss-server e fuss-client
********************************************************************

Per fare test di fuss-server e fuss-client è utile avere a disposizione
delle macchine virtuali; specialmente se si lavora su debian stretch o
successive (dove VirtualBox non è più disponibile) è comodo usare
qemu/kvm tramite libvirt.

Installazione e configurazione
==============================

Installazione di libvirt
------------------------

* Installare i seguenti pacchetti::

   apt install qemu libvirt-clients libvirt-daemon virtinst \
   libvirt-daemon-system virt-viewer virt-manager dnsmasq-base

* Aggiungere il proprio utente ai gruppi ``libvirt`` e ``kvm``::

   adduser $UTENTE libvirt
   adduser $UTENTE kvm

Una volta che l'utente fa parte dei gruppi (ad esempio previo
logout/ri-login) si può usare ``virt-manager`` per gestire macchine
virtuali e reti tramite un'interfaccia grafica simile a quella di
VirtualBox.

Volendo invece usare la riga di comando, si può proseguire con questa
guida.

Configurazione della rete
-------------------------

fuss-server richiede la configurazione di almeno due, in alcuni casi
tre, schede di rete: una con accesso ad internet e due su rete isolata.

Per creare queste interfacce di rete da riga di comando se ne deve
scrivere il file di configurazione e passarlo al comando ``virsh
net-define``

Per la rete nattata, creare il file ``natted.xml`` con i seguenti
contenuti, sostituendo a ``8.8.8.8`` l'indirizzo di un server dns opportuno::

   <network>
     <name>natted</name>
     <forward mode='nat' />
     <bridge name='virbr7' stp='on' delay='0'/>
     <dns>
       <forwarder addr='8.8.8.8'/>
     </dns>
     <ip address='192.168.7.1' netmask='255.255.255.0'>
       <dhcp>
         <range start='192.168.7.128' end='192.168.7.254'/>
       </dhcp>
     </ip>
   </network>

.. tip:: Con questa configurazione sull'host verrà configurata
   un'interfaccia di rete ``virbr7`` con assegnato l'indirizzo
   ``192.168.7.1``.

   Le macchine virtuali avranno invece a disposizione un dchp che
   assegnerà loro indirizzi nel range da ``192.168.7.128`` a
   ``192.168.7.254``, e le richieste DNS verranno inoltrate al server
   ``8.8.8.8.``.

quindi lanciare, come root::

   # virsh net-define natted.xml
   # virsh net-start natted

Similmente, per le interfacce isolate si può usare quanto segue, nel
file ``isolated.xml``::

   <network>
     <name>isolated</name>
     <bridge name='virbr6' stp='on' delay='0'/>
     <ip address='192.168.6.253' netmask='255.255.255.0'>
     </ip>
   </network>

e nel file ``isolated2.xml``::

   <network>
     <name>isolated2</name>
     <bridge name='virbr8' stp='on' delay='0'/>
     <ip address='192.168.8.253' netmask='255.255.255.0'>
     </ip>
   </network>

E come prima, sempre con utente root::

   # virsh net-define isolated.xml
   # virsh net-start isolated
   # virsh net-define isolated2.xml
   # virsh net-start isolated2

In questo modo le interfacce sono definite, ma non verranno avviate
automaticamente; per farlo usare i seguenti comandi::

   # virsh net-autostart natted
   # virsh net-autostart isolated
   # virsh net-autostart isolated2

oppure usare ``net-start`` per ciascuna interfaccia quando se ne ha bisogno.

Creazione delle macchine virtuali
---------------------------------

Per creare la macchina che ospiterà il server, dopo aver abilitato le
interfacce di rete e scaricato l'`iso di fuss-server
<http://iso.fuss.bz.it/>`_ lanciare il seguente comando::

   $ virt-install --connect qemu:///system --name fuss_server --memory 1024 \
     --cdrom $PATH_ISO_FUSS-SERVER --network network=natted \
     --network network=isolated --network network=isolated2 \
     --disk size=16,format=qcow2 --os-variant debian12

questo creerà una macchina virtuale che fa il boot dall'iso
dell'installer e aprirà una finestra di virt-viewer per controllarla.
Alla fine dell'installazione si può fare login e procedere con
l'`Installazione di Fuss Server
<https://fuss-tech-guide.readthedocs.io/it/latest/quick-install.html>`_

Una volta che il server è installato e configurato si può fare la stessa
cosa per una (o più) macchine client::

   $ virt-install --connect qemu:///system --name fuss_client --memory 1024 \
     --cdrom $PATH_ISO_FUSS-CLIENT --network network=isolated \
     --disk size=24,format=qcow2 --os-variant debian12

Gestione
========

Boot delle macchine
-------------------

Per avviare le volte successive le macchine è necessario:

* Se l'host è stato spento, e non è stato configurato l'autoavvio delle
  interfacce di rete, abilitarle::

   # virsh net-start natted
   # virsh net-start isolated
   # virsh net-start isolated2

* Avviare la macchina di cui si ha bisogno::

   $ virsh --connect qemu:///system start fuss-server

* Se necessario, avviare una sessione grafica sulla macchina::

   $ virt-viewer --connect qemu:///system fuss-server

Snapshot
--------

Per creare uno snapshot di una macchina::

   virsh -c qemu:///system snapshot-create-as fuss-server <nome> "<descrizione>"

Per vedere l'elenco degli snapshot disponibili::

   virsh -c qemu:///system snapshot-list fuss-server

Per riportare la macchina ad uno snapshot, eliminando tutte le modifiche
effettuate nel frattempo::

   virsh -c qemu:///system snapshot-revert fuss-server <nome>

oppure, per usare lo snapshot corrente (sempre perdendo le modifiche)::

   virsh -c qemu:///system snapshot-revert fuss-server --current

Configurazioni del sistema
==========================

Configurazione della rete
-------------------------

All'interno del fuss-server, le interfacce di rete come definite in
questa pagina possono essere configurate aggiungendo a
``/etc/network/interfaces`` le seguenti righe::

   allow-hotplug enp1s0
   iface enp1s0 inet dhcp

   allow-hotplug enp2s0
   iface enp2s0 inet static
       address 192.168.6.1
       netmask 255.255.255.0

In fuss-client non è invece necessaria nessuna configurazione, dato che
viene usato dhcp come da default.

Vedi anche
==========

In caso di problemi o per approfondire l'uso da riga di comando di
libvirt è molto utile la `pagina su Libvirt della wiki di Arch Linux
<https://wiki.archlinux.org/index.php/Libvirt>`_ , la maggior parte
della quale si applica anche a sistemi Debian o Debian-based.
