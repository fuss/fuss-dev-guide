*************
Metapacchetti
*************

La distribuzione FUSS comprende alcuni metapacchetti per semplificare
l'installazione di programmi didattici o comunque utili in ambito
scolastico, gestiti nel `progetto fuss-software
<https://work.fuss.bz.it/projects/fuss-software>`_

Repository
==========

Il repository dei metapacchetti si può clonare con::

    $ git clone https://work.fuss.bz.it/git/fuss-software

Il branch ``master`` si riferisce all'ultima release di FUSS, le
versioni precedenti sono nei branch chiamati con il codename della
distribuzione relativa.

Modifica dei metapacchetti
==========================

I file della cartella ``metapackages`` si riferiscono al metapacchetto
con lo stesso nome e contengono le dipendenze, una per riga e
preferibilmente in ordine alfabetico.

.. warning:: Le dipendenze devono essere presenti all'interno dei
   repository configurati, altrimenti il pacchetto non sarà più
   installabile.

   A luglio 2018 questo significa che i pacchetti che si desidera
   installare devono essere presenti in Debian 9 “stretch” nelle sezioni
   ``main`` e ``contrib``.

Numero di versione
------------------

La major version dei metapacchetti deve rispecchiare la versione della
distribuzione Debian utilizzata; ad esempio un pacchetto per la versione
“stretch” avrà come major version 9.

Il patch level va aumentato di uno ad ogni versione, come di consueto.

I metapacchetti sono nativi, quindi non deve essere presente una
versione debian, ma solo le tre componenti MAJOR.MINOR.PATCH.

Per build del pacchetto e upload delle modifiche vedere
:doc:`pacchetti-e-repository`
