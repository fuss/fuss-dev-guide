************************
Nuove versioni di Debian
************************

Questa procedura è la procedura per realizzare l'aggiornamento di una
versione di FUSS basata su una nuova release Debian stable.

Procedura di aggiornamento
==========================

Repository software
-------------------

I repository git contenenti il software custom di FUSS dovranno ospitare
le nuove versione specifiche per la versione upstream.

Il workflow attuale è il seguente:

branch ``master``:
   attuale distribuzione debian stable
branch ``codename_distribuzione``:
   contiene il codice rispetto ad una specifica versione di Debian, per
   backporting e altre correzioni

Quando viene rilasciata una nuova Debian, è necessario quindi creare il
branch relativo alla nuova oldstable, e proseguire su master per
l'attuale versione stabile.

Ad esempio, per passare da stretch a buster, su tutti i repository che
contengono software pubblicato nell'archivio di FUSS::

   (master) $ git pull
   (master) $ git checkout -b stretch
   (stretch) $ git push -u origin stretch
   (stretch) $ git checkout master
   (master) $ # proseguire con le modifiche...

Aggiornamento, build e test pacchetti
-------------------------------------

Ogni nuovo pacchetto dovrà come minimo:

* Essere aggiornato allo standard di riferimento Debian per la versione
  in uso (vedi https://www.debian.org/doc/debian-policy/ ).
* Avere una versione (e relativo changelog) che riporti, come major
  version number, quello della distribuzione Debian di riferimento. Ad
  esempio un pacchetto per Debian Buster avrà come versione 10.x.x.
* Essere aggiornato rispetto alle nuove versioni delle dipendenze
  presenti nella nuova versione della distribuzioone.
* Dovrà essere buildato *e testato* su una installazione della versione
  Debian di riferimento.

Per le istruzioni di build e di successivo upload si veda
:doc:`pacchetti-e-repository`.

fuss-server e fuss-client
^^^^^^^^^^^^^^^^^^^^^^^^^

:doc:`fuss-server` e :doc:`fuss-client` intervengono sui file di
configurazione di numerosi pacchetti: per il loro aggiornamento è
opportuno fare delle verifiche specifiche sul loro funzionamento.

* Per i file in cui vengono inserite sezioni nei file di configurazione
  (task ``lineinfile`` e ``blockinfile``) che le sezioni siano ancora
  inserite nel posto opportuno.
* Per i file che vengono completamente sovrascritti (task ``copy`` e
  ``template``) è generalmente il caso di ripartire dal file di
  configurazione di default della nuova versione di debian e riapplicare
  le modifiche necessarie (in modo da ricevere le nuove eventuali
  impostazioni di default).

Immagini ISO
------------

TBD

Informazioni sullo sviluppo upstream
====================================

Date di release
---------------

Contrariamente ad altre distribuzioni, Debian non fissa date di
rilascio, dando maggiore priorità alla qualità della distribuzione.
Vengono però fissate le date di *freeze*, il che permette di stimare con
approssimazione di qualche mese quando avverrà il prossimo rilascio.

La freeze, avviene ogni due anni d'inverno; le date precise sono
annunciate con largo anticipo e pubblicate su
https://release.debian.org/; i rilasci avvengono generalmente
[#rilasci]_  durante le estati degli anni dispari.

.. [#rilasci] Ulteriori statistiche sono presenti su
   https://wiki.debian.org/DebianReleases#Release_statistics
   Notare che la data della freeze è stata spostata in avanti di due
   mesi tra jessie e stretch.

A partire dalla data della freeze non vengono più effettuati cambiamenti
delle versioni di pacchetti presenti nella distribuzione, ma vengono
solo accettati fix mirati per bug sufficientemente importanti; questo
per FUSS vuol dire che eventuali test di aggiornamento di versione
saranno già corrispondenti al comportamento della versione rilasciata.
