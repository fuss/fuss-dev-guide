**********************
Pacchetti e Repository
**********************

La distribuzione FUSS comprende un repository di pacchetti aggiuntivi
rispetto alla base (Debian), disponibile all'indirizzo
https://archive.fuss.bz.it/ ed ospitato su ``isolda.fuss.bz.it`` nella
directory ``/iso/repo``.

.. contents::

Build dei pacchetti
===================

Nei repository del software sviluppato per FUSS è presente la directory
``debian`` contenente i file necessari per la generazione dei pacchetti
``.deb``.

Nei progetti sufficientemente recenti, tale directory è presente solo in
un branch dedicato, con un nome tipo ``fuss/<versione>``; per questi
casi vedere anche la sezione :ref:`pacchettizzazione-git` .

Setup
-----

Per effettuare build locali dei pacchetti è necessario installare alcuni
strumenti di sviluppo::

    # apt install devscripts dput-ng dh-systemd dh-python

.. note:: Assicurarsi di aver installato anche i Recommends dei
   pacchetti (questo è il comportamento di default di ``apt``, a meno
   che non lo si sia disabilitato manualmente), in particolare nel caso
   di ``dput-ng``.

Inoltre è necessario impostare le variabili di ambiente ``DEBEMAIL`` e
``DEBFULLNAME``, contenenti rispettivamente il nome completo e l'email
dello sviluppatore, che verranno usate per aggiornare alcuni metadati.

Per usare ``dput-ng`` per effettuare gli upload serve configurarlo
creando il file ``~/.dput.d/profiles/fuss-<versione>.json`` contenente::

    {
        "method": "sftp",
        "fqdn": "archive.fuss.bz.it",
        "incoming": "/iso/incoming/<versione>",
        "allow_dcut": false,
        "allowed-distribution": {},
        "codenames": null,
        "post_upload_command": "ssh -S none isolda.fuss.bz.it 'sudo /iso/bin/post-upload'",
        "hooks": [
            "allowed-distribution",
            "checksum",
            "suite-mismatch"
        ]
    }

dove ``<versione>`` è ``bookworm`` e ``bookworm-proposed-updates`` per
la versione corrente di FUSS server e client, e ``buster`` e
``buster-proposed-updates`` per la versione legacy del server e
``bullseye`` e ``bullseye-proposed-updates`` per quella del client.

.. tip:: in alcune versioni di dput-ng è presente un bug, `#952576
   <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=952576>`_ per cui
   eventuali errori di ``post-upload`` non vengono riportati, rendendo
   silenzioso il fallimento. Se si incappa in quella situazione, un
   possibile workaround è sostituire la configurazione di
   ``post_upload_command`` con la seguente::

      "post_upload_command": "ssh -S none isolda.fuss.bz.it '/iso/bin/post-upload 2>&1'",

   in modo che eventuali errori vengano rediretti su stdout e vengano
   visualizzati.

Assicurarsi inoltre di poter accedere via ssh ad ``archive.fuss.bz.it`` senza
ulteriori opzioni; ad esempio potrebbe essere necessario aggiungere
quanto segue a ``~/.ssh/config``::

    Host archive.fuss.bz.it
        User root

.. note::  dput-ng usa paramiko per effettuare le connessioni ssh;
   questo implica che le opzioni impostate direttamente in
   ``~/.ssh/config`` vengono lette correttamente, ma non c'è supporto
   per l'uso di ``Include`` per suddividere la configurazione su più
   file.

   Inoltre non verrà salvato il fingerprint dei server, ma verrà chiesto
   ogni volta di verificarlo.

   A marzo 2019 i fingerprint di isolda sono::

       256 SHA256:aLTgA+Trj5iYo0dl0i8Q82aigs3K/dPwDbazrvG95YY root@isolda (ECDSA)
       256 SHA256:7i6j0jXPWRrW6LXDGbR+HWr3AFJi6gGSmdW41uBRJV4 root@isolda (ED25519)
       2048 SHA256:OkP1maDf0pSIGCdq1mph8oI8CTADMrFXfe3aty608SA root@isolda (RSA)

       256 MD5:b1:a1:ec:cb:a5:39:c8:8d:39:f1:dd:ba:aa:be:38:11 root@isolda (ECDSA)
       256 MD5:21:41:8b:19:1b:25:b5:9c:f2:5c:e8:b9:8b:08:07:f8 root@isolda (ED25519)
       2048 MD5:bd:88:bd:5f:bc:52:03:0b:88:d9:0c:2b:86:59:dc:92 root@isolda (RSA)

Cowbuilder
^^^^^^^^^^

.. note::
   cowbuilder e pbuilder sono degli strumenti per gestire delle chroot
   all'interno delle quali effettuare build di pacchetti in un ambiente
   pulito e abbastanza isolato dal sistema base.

   Buildare pacchetti all'interno di un sistema isolato è utile per
   evitare influenze da parte del proprio sistema (con librerie ed altre
   dipendenze già installate, magari in versioni non standard), ma è
   anche comodo nel caso si vogliano generare pacchetti per
   distribuzioni diverse da quelle in uso (ad esempio buildare per
   buster su un sistema bookworm)

Oltre a quanto indicato sopra, installare ``cowbuilder``, ``pbuilder`` e
``git-buildpackage``::

   # apt install pbuilder cowbuilder git-buildpackage

ed assicurarsi che l'utente che si vuole usare per lanciare le build
faccia parte del gruppo ``sudo``.

Quindi creare le chroot base per le distribuzioni attualmente (gennaio
2023) in uso buster, bullseye e bookworm::

   # cowbuilder --create --distribution buster --debootstrap debootstrap \
     --basepath /var/cache/pbuilder/base-fuss-buster.cow
   # cowbuilder --create --distribution bullseye --debootstrap debootstrap \
     --basepath /var/cache/pbuilder/base-fuss-bullseye.cow
   # cowbuilder --create --distribution bookworm --debootstrap debootstrap \
     --basepath /var/cache/pbuilder/base-fuss-bookworm.cow

.. tip::
   cowbuilder può essere usato anche sotto distribuzioni derivate da
   debian, come ubuntu; in questo caso è necessario però specificare
   esplicitamente l'uso di un mirror debian aggiungendo ai comandi sopra
   le opzioni::

      --mirror http://<un mirror debian valido>/debian --components main

   inoltre per il funzionamento è necessario disporre delle chiavi di firma
   GPG di Debian, che in genere si installano con::

     apt-get install -y debian-archive-keyring

Aggiungere i repository di backports e fuss alle chroot base appena
create: copiare il file di chiave nella chroot::

   # wget -O \
     /var/cache/pbuilder/base-fuss-bookworm.cow/usr/share/keyrings/fuss-keyring.gpg \
     https://archive.fuss.bz.it/apt.gpg

quindi fare login nella chroot::

   # cowbuilder --login --save-after-login \
     --basepath /var/cache/pbuilder/base-fuss-bookworm.cow

ed effettuare le modifiche a ``/etc/apt/sources.list`` (sostituendo
``<mirror>`` con un mirror debian opportuno, ad esempio quello già
presente in ``/etc/apt/sources.list``::

   # echo 'deb <mirror> bookworm-backports main' >> /etc/apt/sources.list
   # echo 'deb [signed-by=/usr/share/keyrings/fuss-keyring.gpg] http://archive.fuss.bz.it/ bookworm main contrib' \
     >> /etc/apt/sources.list
   # apt update
   # exit

fino alla versione buster le istruzioni erano leggermente diverse::

   # echo 'deb <mirror> buster-backports main' >> /etc/apt/sources.list
   # echo 'deb http://archive.fuss.bz.it/ buster main contrib' \
     >> /etc/apt/sources.list
   # apt install gnupg
   # apt-key add - # incollare i contenuti di
                   # https://archive.fuss.bz.it/apt.key seguiti da ctrl-d
   # apt remove gnupg
   # apt autoremove
   # apt update
   # exit

.. note::

   nella chroot minimale da stretch in poi non è presente gnupg, che è
   necessario per l'uso di apt-key add: lo installiamo per l'operazione
   e rimuoviamo subito dopo per essere certi che l'immagine sia sempre
   minimale e continuare ad accorgersi di eventuali dipendenze non
   esplicitate nei pacchetti che generiamo.

Ripetere la stessa cosa per le altre eventuali chroot.

Nel caso in cui le chroot siano state create da un po' di tempo è
opportuno aggiornarle, coi seguenti comandi::

   # cowbuilder --update --basepath /var/cache/pbuilder/base-fuss-buster.cow/
   # cowbuilder --update --basepath /var/cache/pbuilder/base-fuss-bullseye.cow/
   # cowbuilder --update --basepath /var/cache/pbuilder/base-fuss-bookworm.cow/

Clone e/o aggiornamento del repository
--------------------------------------

Clonare il repository del progetto desiderato::

   $ git clone https://work.fuss.bz.it/git/<progetto>


Nei vecchi progetti il branch da buildare per l'upload è ``master``, in
quelli recenti ``fuss/*`` (``fuss/master`` o altro a seconda del
target), in entrambi i casi da aggiornare nel caso in cui si abbia già
un clone locale del repository::

   $ git checkout [fuss/]master
   $ git pull

Nel caso si stia facendo una release per un pacchetto recente
(pacchettizzazione in ``fuss/*``) ricordarsi di aggiornare il branch con
le modifiche che si vogliono integrare nella release.

.. hint::
   Ad esempio, per fare una release della versione di sviluppo attivo di
   un pacchetto recente, si inizia assicurandosi di avere la versione
   corrente del branch di sviluppo, ``master``::

      $ git checkout master
      $ git pull

   si controlla eventualmente che il numero di versione in ``setup.py``
   sia corretto, ed eventualmente lo si aggiorna e committa::

      $ $EDITOR setup.py
      $ git add -p setup.py
      [...]
      $ git commit -m 'Bump version to <version>'
      $ git push

   si passa al branch con la pacchettizzazione, assicurandosi che anche
   questo sia aggiornato::

      $ git checkout fuss/master
      $ git pull

   e si fa il merge del branch di sviluppo::

      $ git merge master
      $ git push

   a questo punto si procede come descritto al prossimo punto
   aggiornando il changelog del pacchetto, eccetera.

Versionamento
-------------

Per poter pubblicare il pacchetto, è necessario incrementare il numero
di versione nel file ``debian/changelog``.

Il numero di versione da dare dipende dal tipo di pacchetto, come
descritto nella sezione :ref:`versionamento` e nelle guide di sviluppo
degli specifici pacchetti, ma nella maggior parte dei casi sarà da
incrementare il patch level (es. da 10.0.5-1 a 10.0.6-1).

.. note:: Nei pacchetti contenenti programmi in python è generalmente
   necessario mantenere aggiornato il numero di versione anche in
   ``setup.py``; come per debsrc sopra questo dovrebbe essere citato nel
   README dei pacchetti.

   Notare che l'aggiornamento del numero di versione in ``setup.py`` va
   effettuato nel branch di sviluppo, e non in quello di
   pacchettizzazione.

Il programma ``dch``,  permette di automatizzare l'editing del file
``debian/changelog`` che contiene la versione del pacchetto.

* Quando si iniziano a fare modifiche usare il comando ``dch -v
  <nuova_versione>`` per creare una nuova stanza ed aprire il changelog
  nell'editor di default.

  Verrà impostato il numero di versione richiesto e la release speciale
  ``UNRELEASED`` che indica che le modifiche sono ancora in lavorazione.

  Si può anche usare ``dch`` senza opzioni: in questo modo se l'ultima
  stanza risulta ``UNRELEASED`` il file verrà aperto così com'è, mentre
  se l'ultima stanza riporta una release come ``unstable`` ne viene
  creata una nuova incrementando il numero di versione.

  Attenzione che in quest'ultimo caso dch potrebbe non essere in grado
  di indovinare la versione corretta: verificare e nel caso correggere.
  Inoltre, nel caso in cui non si sia elencati tra i Maintainer e
  Uploaders in ``debian/control`` verrà aggiunta una riga ``Non
  Maintainer Upload`` che per noi non è rilevante e va tolta.

  Nel caso in cui più persone facciano modifiche, dch provvederà a
  suddividerle in sezioni intestate con il nome della persona che ha
  effettuato la modifica.

* Descrivere le modifiche effettuate, possibilmente indicando i ticket
  di riferimento da cui nascono le richieste di modifica.

* Man mano che si fanno modifiche, descriverle se necessario nel
  changelog, usando ``dch`` senza opzioni, come descritto sopra.

* Quando si è pronti a pubblicare il pacchetto, modificare ``UNRELEASED``
  con ``unstable`` nella prima riga; questo si può fare anche con il
  comando ``dch -r``.

Verifica dello stato del repository e push
------------------------------------------

Prima di effettuare la build, accertarsi di aver committato tutte le
modifiche effettuate, di non avere file spuri e di essere sul branch
corretto (``master`` o ``fuss/*`` a seconda dell'età del progetto),
ad esempio con il comando::

   $ git status

Committare quindi eventuali modifiche rimanenti, facendo attenzione a
mantenere distinte le modifiche di sviluppo da quelle di
pacchettizzazione, indicando se possibile nel commit log il numero di
ticket associato alla modifica, con la dicitura "refs #NUMERO"::

   $ git add -p <file modificati>
   $ git add <file aggiunti>
   $ git commit -m "<modifiche effettuate>. refs #NumeroTicket"

Inoltre o subito prima o subito dopo la build, ma prima dell'upload, è
importante pushare tali commit, in modo da essere sicuri che nel
frattempo non avvengano conflitti con commit altrui::

   $ git push

Build
-----

Alcuni pacchetti, come octofussd necessitano della preventiva creazione
del tar dei sorgenti originali, come specificato nel README dei
rispettivi repository; in tal caso prima di eseguire il comando
precedente è necessario eseguire, nella directory principale del
repository::

   $ debian/rules debsrc

A questo punto si può usare ``pdebuild`` per eseguire la build del
pacchetto all'interno di una chroot opportuna gestita da cowbuilder
(sostituendo ``bookworm`` con ``buster`` o ``bullseye`` nei casi
opportuni)::

   $ DIST=bookworm pdebuild --use-pdebuild-internal --pbuilder cowbuilder -- --basepath /var/cache/pbuilder/base-fuss-bookworm.cow/

``pdebuild`` provvederà autonomamente ad installare le dipendenze
necessarie all'interno della chroot e ad effettuare la build.

Generalmente, l'infrastruttura di build [#infrastruttura]_ è in grado di
capire dal numero di versione ed altri indizi se sia necessario o meno
includere la tarball ``.orig`` tra ciò che va uploadato.
Nel caso in cui però si stia effettuando un backport questo non è
automatico: per il primo backport di una certa versione upstream è
necessario prevedere l'inclusione della tarball sorgente con l'opzione
``--debbuildopts "-sa"``, ovvero::

   $ DIST=bookworm pdebuild --buildresult ../build/ --use-pdebuild-internal --pbuilder cowbuilder --debbuildopts "-sa" -- --basepath /var/cache/pbuilder/base-fuss-bookworm.cow/

.. note::
   Alla fine della build si riceverà un warning ``cannot create regular
   file /var/cache/pbuilder/result/<nomepacchetto>_<versione>.dsc``;
   questo è irrilevante e i file necessari che sono stati generati si
   trovano nella directory superiore a quella da cui è stato lanciato
   ``pdebuild``.

.. [#infrastruttura] In particolare, l'inclusione o meno della tarball sorgente è
   decisa ed effettuata da ``dpkg-genchanges``, richiamato da
   ``dpkg-buildpackage`` al quale ``pdebuild`` passa le opzioni
   specificate con il parametro ``--debbuildopts``.

   Generalmente questo avviene in automatico, senza bisogno di
   preoccuparsi di chi faccia cosa.

Build con git-buildpackage
^^^^^^^^^^^^^^^^^^^^^^^^^^

In alternativa all'uso diretto di ``pdebuild``, ma solo per i progetti
il cui repository lo supporti tramite l'uso di un branch separato per la
pacchettizzazione, è possibile usare ``git-buildpackage`` (o ``gbp``):
oltre ad effettuare la build in una chroot minimale questo si assicura
anche che non ci siano differenze tra quanto committato (localmente) e
quanto viene usato per la build.

Per effettuare la build in questo caso è necessario spostarsi sul branch
di pacchettizzazione, ad esempio::

   $ git checkout fuss/master

eventualmente riportare in tale branch le modifiche effettuate su
master::

   $ git merge master

e quindi si può buildare, nel caso generale con::

   gbp buildpackage --git-pbuilder --git-debian-branch=fuss/master \
      --git-dist=fuss-bookworm --git-no-pristine-tar

Per forzare l'inclusione della tarball sorgente dei backports, come
spiegato sopra, in questo caso è sufficiente aggiungere ``-sa``.

Test
----

Tramite ``apt install ./<nomefile>.deb`` si puo' installare e testare il
pacchetto.  Notare l'uso di ``./`` per specificare un file locale.

Un altro comando utile è ``dpkg -c <nomefile>.deb`` per verificare i
file presenti nel pacchetto.

lintian
^^^^^^^

Uno strumento di diagnostica molto dettagliato è ``lintian``, che
analizza i pacchetti generati alla ricerca di problemi di vario tipo e
si lancia con::

   lintian --pedantic -Iiv <pacchetto>.changes

Un limite di questo strumento è che è basato sugli standard di Debian e
in alcuni casi gli errori potrebbero essere falsi positivi per gli
standard fuss.

In particolare si possono ignorare i seguenti tag.

* ``changelog-should-mention-nmu``
* ``source-nmu-has-incorrect-version-number``

ed altri che verranno successivamente aggiunti a questo elenco.

Upload
------

Per uploadare il pacchetto buildato con ``dput-ng`` è sufficiente usare
il comando::

    $ dput fuss-<versione> nomepacchetto_versione_arch.changes

Nel caso si voglia procedere manualmente invece si possono copiare i
file generati su ``isolda`` nella directory ``/iso/incoming/<versione>``
ed aggiornare il repository con il comando::

    # /iso/bin/post-upload

Verificare poi che in ``/iso/incoming/<versione>`` non siano rimasti
file spuri, e nel caso cancellarli a mano.

Tagging
-------

Nel momento in cui tutto è pronto per un upload, taggare il commit
corrispondente a quanto verrà uploadato con il comando::

   $ git tag -s -m 'Fuss release <versione>' fuss/<versione>

in questo modo il tag verrà firmato con la propria chiave gpg di
default; per non firmare il tag::

   $ git tag -a -m 'Fuss release <versione>' fuss/<versione>

Ricordarsi di effettuare il push dei tag verso il server::

   $ git push --tags

.. _chroot:

Build dei pacchetti in chroot
=============================

Nel caso ci siano problemi con l'uso di cowbuilder, è anche possibile
usare una semplice chroot all'interno della quale installare gli
strumenti di build e clonare il pacchetto.

Setup
-----

Per creare una chroot ed installare gli strumenti di base::

    # mkdir <versione>_build
    # debootstrap <versione> <versione>_build (<mirror>)
    # chroot <versione>_build
    # apt install debhelper devscripts dpkg-dev

dove ``<versione>`` è al momento (gennaio 2023) ``bookworm`` per FUSS
server e client correnti, ``buster`` per la versione legacy del FUSS
server e ``bullseye`` per quella del FUSS client.

Build
-----

Una volta clonato il repository (dentro la chroot), incrementato il
numero di versione come sopra ed eventualmente generato il tar sorgente,
per eseguire il build del pacchetto eseguire, nella directory principale
del repository::

    # dpkg-buildpackage -us -uc

Se la procedura va a buon fine, nella directory superiore si troveranno
i pacchetti ``.deb`` generati, e anche i file ``.changes``, ``.dsc`` e
``.tar.gz`` con il sorgente del pacchetto.

La procedura potrebbe fallire con un errore contenente::

    Unmet build dependencies: <pacchetto1> <pacchetto2>

in tal caso installare semplicemente i pacchetti e riprovare.
Tali dipendenze sono elencate nel campo ``Build-Depends`` del file
``debian/control``, nel caso ci si voglia assicurare di averle già
installate prima di buildare.

A questo punto si può procedere con test, commit+push ed upload come nel
caso generale.

.. _versionamento:

Policy di versionamento
=======================

Software sviluppato per FUSS
----------------------------

Per i pacchetti sviluppati specificatamente per FUSS possono esserci
policy specifiche indicate nella relativa guida sviluppatori e/o nei
README dei progetti.

In generale, lo schema usato prevede che la major version corrisponda
alla versione di fuss per cui è rilasciato il pacchetto (che a sua volta
corrisponde alla versione di debian su cui è basta).
Un pacchetto per FUSS 9 avrà quindi versione tipo 9.X.Y, uno per FUSS 10
10.X.Y eccetera.

I pacchetti possono essere nativi o meno: nel primo caso il numero di
versione è del tipo 10.X.Y sia per il pacchetto che in ``setup.py``,
mentre nel secondo si aggiunge un numero di revisione, es. 10.X.Y-Z;
quest'ultimo va incrementato quando la nuova versione presenta modifiche
nella pacchettizzazione (ovvero nella directory debian), ma non nel
codice.

I pacchetti nativi devono anche avere ``3.0 (native)`` nel file
``debian/source/format``, mentre i pacchetti non-nativi devono avere
``3.0 (quilt)`` e per buildarli è necessario generare una tarball
sorgente (``<nome>_<10.X.Z>.orig.tar.gz``), ad esempio tramite
``debsrc``.

Rebuild di pacchetti di debian
------------------------------

Per i pacchetti presi da debian e ribuildati da noi seguiamo una
convenzione simile a quella usata dai backports_ aggiungendo ``~fussN-X``
al numero di versione, dove N è la versione di FUSS per la quale stiamo
preparando il pacchetto e X la revisione del backport.

.. _backports: https://backports.debian.org/

Se si fa una rebuild di un pacchetto che ad esempio ha versione 1.2.3-4
la nostra versione sarà 1.2.3-4~fuss10+1 (+2 per una rebuild successiva
con modifiche alla sola pacchettizzazione, eccetera).

Configurazione del repository
=============================

Il file ``/iso/repo/conf/distributions`` definisce le distribuzioni
utilizzate nel repository, con snippet di configurazione come::

    Origin: FUSS
    Label: FUSS
    Suite: bookworm
    Codename: bookworm
    Version: 10.0
    Architectures: i386 amd64 source
    Components: main contrib
    Description: FUSS 10.0
    SignWith: C00D47EF47AA6DE72DFE1033229CF7A871C7C823

inoltre nello stesso file sono definite le versioni precedenti e future
della distribuzione. Al momento attuale la configurazione riguarda fino
alla versione 12 di Debian (codename ``bookworm``).

Le varie distribuzioni sono raggiungibili da apt usando, in
``/etc/apt/sources.list``::

    deb http://archive.fuss.bz.it CODENAME_DISTRIBUZIONE main contrib

e la chiave con la quale viene firmato il repository si può installare
su una macchina debian o derivate eseguendo, da root::

   # wget -qO /usr/share/keyrings/fuss-keyring.gpg https://archive.fuss.bz.it/apt.key

Aggiunta di nuova distribuzione e/o nuovo repository
----------------------------------------------------

Oltre al file ``/iso/repo/conf/distributions`` per indicare la nuova
distribuzione e/o nuovo repository, è necessario:

* Creare una cartella per lo spool di incoming dei pacchetti in
  ``/iso/incoming/<nuova distribuzione>``
* Aggiungere la descrizione e il path relativo al punto precedente nel
  file ``/iso/repo/conf/incoming``
* Aggiungere allo script ``/iso/bin/post-upload`` l'esecuzione del
  processing del nuovo path di incoming. In questo script vanno tolte
  quelle non più usate quando è certo che non ci saranno più nuovi
  pacchetti per una specifica distribuzione.

Copia di pacchetti tra distribuzioni diverse
--------------------------------------------

reprepro permette di copiare dei pacchetti già caricati per una
distribuzione in una distribuzione diversa; questo è utile ad esempio
per portare un pacchetto da ``<distro>-proposed-updates`` a ``<distro>``
una volta che si è appurato il corretto funzionamento.

Il comando è::

   root@isolda:/iso/repo# reprepro copy bookworm bookworm-proposed-updates <nome_pacchetto>

dove si deve fare attenzione che la *prima* distribuzione specificata è
quella di destinazione, seguida dalla distribuzione di origine.

.. _pacchettizzazione-git:

Verifica delle versioni presenti sul repository
-----------------------------------------------

Per sapere che versioni di un pacchetto sono presenti in che
distribuzione basta usare il comando::

   reprepro ls <nomepacchetto>

Pacchettizzazione gestita con git
=================================

Come descritto nelle istruzioni, nei progetti più recenti si è adottata
una delle convenzioni in uso in Debian per la pacchettizzazione basata
su git.

* I branch di sviluppo del progetto, incluso ``master`` non contengono
  la directory ``debian``, come da raccomandazione della `UpstreamGuide
  <https://wiki.debian.org/UpstreamGuide#Pristine_Upstream_Source>`_ di
  Debian.
* I branch il cui nome inizia per ``fuss/`` contengono la directory
  debian; generalmente il branch usato per gli upload della versione
  corrente sarà ``fuss/master``.
* Ad ogni rilascio, il branch ``master`` viene mergiato in
  ``fuss/master`` (ma *mai* il contrario) e il pacchetto può essere
  generato con i metodi descritti sopra.

Nel caso si voglia effettuare la build con ``gbp`` (pacchetto
``git-buildpackage`` il comando da usare sarà::

   gbp buildpackage \
   --git-pbuilder \
   --git-no-pristine-tar \
   --git-debian-branch=fuss/<versione> \
   --git-dist=fuss-bookworm

aggiungendo ``--git-export=WC`` per fare build di prova dello stato
attuale della working directory (anziché dello stato all'ultimo commit)
oppure ``--git-ignore-new`` per fare una build corrispondente all'ultimo
commit, ignorando le modifiche eventualmente presenti.

Configurazioni standard e nuovi pacchetti
=========================================

Maintainer
----------

Il campo ``Maintainer`` di ``debian/control`` deve avere come valore
``FUSS team <packages@fuss.bz.it>``, in modo da indicare che il
pacchetto è manutenuto da un team.


Pacchetti particolari
=====================

coova-chilli
------------

Il pacchetto coova-chilli presente su archive.fuss.bz.it è generato da
un nostro repository https://gitlab.fuss.bz.it/fuss/coova-chilli/ copia
del repository upstream https://github.com/coova/coova-chilli.git alla
quale abbiamo aggiunto alcune modifiche di pacchettizzazione.

In particolare, per la release 1.6 è presente un branch ``1.6-patched``
basato sul tag upstream 1.6 con l'aggiunta dell'opzione di compilazione
``--enable-wpad``, necessaria per il corretto funzionamento del
fuss-server.

Per effettuare nuove build della versione 1.6 è quindi necessario usare
il branch ``1.6-patched``, mergiandovi eventuali modifiche upstream
desiderate; usando ``git-buildpackage`` si dovrà usare::

   $ gbp buildpackage --git-debian-branch=1.6-patched [--git-pbuilder]

Per versioni successive si deve creare un branch simile dai tag
upstream, riportando le modifiche ancora necessarie.

Altri branch presenti sul nostro repository contengono la
pacchettizzazione per versioni precedenti di FUSS, di utilità solo
storica.

gspeech
-------

Il pacchetto di gspeech presente su archive.fuss.bz.it deriva
direttamente dalla pacchettizzazione presente sul `repository upstream
<https://github.com/lusum/gSpeech>`_, da cui prendere eventuali versioni
aggiornate.
È stata aggiunta soltanto una stanza con il nostro numero di versione,
tipo::

   gspeech (0.9.2.0-1) buster; urgency=medium

     * Rebuild for FUSS 10

    -- Elena Grandi <elena@truelite.it>  Tue, 02 Feb 2021 13:42:20 +0100

avendo cura di incrementare il numero di versione rispetto alle
precedenti.

Vedere anche
============

* https://wikitech.wikimedia.org/wiki/Reprepro#HOWTO
